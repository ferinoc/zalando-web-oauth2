package com.example;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Override
    public UserDetails loadUserByUsername(String username) {

        if (!"admin".equals(username)) {
            throw new UsernameNotFoundException("Username not found " + username);
        }

        return new org.springframework.security.core.userdetails.User("admin",
                new BCryptPasswordEncoder().encode("password"),
                true,
                true,
                true,
                true,
                Collections.emptyList());
    }

}
