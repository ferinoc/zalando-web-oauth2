package com.example.configuration;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.NativeWebRequest;
import org.zalando.problem.Problem;
import org.zalando.problem.spring.web.advice.ProblemHandling;

import java.net.URI;

import static org.zalando.problem.Status.FORBIDDEN;
import static org.zalando.problem.Status.INTERNAL_SERVER_ERROR;
import static org.zalando.problem.Status.UNAUTHORIZED;

@RestControllerAdvice
public class GeneralExceptionHandler implements ProblemHandling {


    @ExceptionHandler
    @Override
    public ResponseEntity<Problem> handleAuthentication(AuthenticationException e, NativeWebRequest request) {

        Problem problem = Problem.builder()
                .withType(URI.create("https://example.org/not-authenticated"))
                .withTitle("Not authenticated")
                .withStatus(UNAUTHORIZED)
                .build();

        return create(e, problem, request);
    }

    @ExceptionHandler
    @Override
    public ResponseEntity<Problem> handleAccessDenied(AccessDeniedException e, NativeWebRequest request) {
        Problem problem = Problem.builder()
                .withType(URI.create("https://example.org/access-denied"))
                .withTitle("access-denied")
                .withStatus(FORBIDDEN)
                .build();

        return create(e, problem, request);
    }


    @ExceptionHandler
    public ResponseEntity<Problem> handleAll(Exception exception, NativeWebRequest request) {
        Problem problem = Problem.builder()
                .withType(URI.create("https://example.org/internal-error"))
                .withTitle("internal error")
                .withStatus(INTERNAL_SERVER_ERROR)
                .build();

        return create(exception, problem, request);
    }

}
