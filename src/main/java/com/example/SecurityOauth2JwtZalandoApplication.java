package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;

@SpringBootApplication(exclude = ErrorMvcAutoConfiguration.class)
public class SecurityOauth2JwtZalandoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecurityOauth2JwtZalandoApplication.class, args);
	}
}
