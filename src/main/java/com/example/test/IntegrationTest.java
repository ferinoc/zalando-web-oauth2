package com.example.test;

import com.example.util.JsonUtil;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class IntegrationTest {

    private static final Logger log = LoggerFactory.getLogger(IntegrationTest.class);

    private RestTemplate restTemplate = new RestTemplate();
    private static final String API_URL = "http://localhost:8080/oauth/token";
    private static final String CLIENT_ID = "client_id";
    private static final String CLIENT_SECRET = "secret";
    private static final String USER_NAME = "admin";
    private static final String USER_PASSWORD = "password";


    @Test
    void provideTokenCorrectCredentials() {
        provideToken(CLIENT_ID, CLIENT_SECRET, USER_NAME, USER_PASSWORD);
    }

    @Test
    void provideTokenWrongUserName() {
        provideToken(CLIENT_ID, CLIENT_SECRET, "wrongUserName", USER_PASSWORD);
    }

    @Test
    void provideTokenWrongUserPassword() {
        provideToken(CLIENT_ID, CLIENT_SECRET, USER_NAME, "wrongUserPassword");
    }

    void provideToken(String clientId, String clientSecret, String userName, String userPassword) {
        HttpEntity<MultiValueMap<String, String>> authorizationEntity = createAuthorizationEntity(clientId, clientSecret, userName, userPassword);
        Object body = restTemplate.exchange(API_URL, HttpMethod.POST, authorizationEntity, Object.class).getBody();
        log.info(JsonUtil.toPrettyJsonString(body));
    }

    private HttpEntity<MultiValueMap<String, String>> createAuthorizationEntity(
            String clientId,
            String clientSecret,
            String userName,
            String userPassword
    ) {
        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("username", userName);
        body.add("password", userPassword);
        body.add("grant_type", "password");
        body.add("scope", "read write");
        body.add("client_id", clientId);
        body.add("client_secret", clientSecret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setBasicAuth(clientId, clientSecret);

        return new HttpEntity<>(body, headers);
    }

}
